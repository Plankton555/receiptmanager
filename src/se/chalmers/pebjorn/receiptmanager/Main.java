package se.chalmers.pebjorn.receiptmanager;

import java.awt.EventQueue;

import javax.swing.UIManager;

import se.chalmers.pebjorn.receiptmanager.view.KvittoApp;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new KvittoApp();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	// TODO Write javadoc for everything (or something to make it easier to understand)
}