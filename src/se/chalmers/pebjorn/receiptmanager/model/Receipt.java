package se.chalmers.pebjorn.receiptmanager.model;

public class Receipt {

	private int year;
	private int month;
	private int day;
	private float price;
	
	public Receipt(int year, int month, int day, float price)
	{
		this.year = year;
		this.month = month;
		this.day = day;
		this.price = price;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}
}