package se.chalmers.pebjorn.receiptmanager.model;

import java.util.Comparator;


public class PriceComparator implements Comparator<Receipt> {

	@Override
	public int compare(Receipt o1, Receipt o2) {
		if (o1.getPrice() == o2.getPrice())
		{
			return 0;
		}
		else if (o1.getPrice() > o2.getPrice())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}