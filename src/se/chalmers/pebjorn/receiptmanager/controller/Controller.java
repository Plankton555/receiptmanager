package se.chalmers.pebjorn.receiptmanager.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import se.chalmers.pebjorn.receiptmanager.model.Receipt;
import se.chalmers.pebjorn.receiptmanager.view.KvittoApp;

public class Controller {

	private KvittoApp view;
	private List<Receipt> receipts;
	private String dbPathName = "database\\data.csv";
	private String nl = System.getProperty("line.separator");
	private JFileChooser fc;
	
	public Controller(KvittoApp kvittoApp) {
		this.view = kvittoApp;
		this.view.setController(this);
		this.fc = new JFileChooser();
		loadAndShowReceipts(dbPathName);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		FileFilter csvFilter = new FileFilter() {
			
			@Override
			public String getDescription() {
				return "CSV files";
			}
			
			@Override
			public boolean accept(File f) {
				if (f.isDirectory())
				{
					return true;
				}
				
				return (f.getName().toLowerCase().endsWith(".csv"));
			}
		};
		fc.setFileFilter(csvFilter);
	}

	private void loadAndShowReceipts(String pathName) {

		this.receipts = loadFromFile(pathName);
		showReceiptList(this.receipts);
	}

	public void addReceipt(int year, int month, int day, Number price) {
		if (price.floatValue() > 0) // TODO Same page error message f�r ogiltigt datum eller ogiltigt v�rde
		{
			Receipt receipt = new Receipt(year, month, day, price.floatValue());
			receipts.add(receipt);
			view.appendToList(receipt);
		}
	}

	private void showReceiptList(List<Receipt> receiptList) {
		view.clearList();
		for (Receipt r : receiptList)
		{
			view.appendToList(r);
		}
		view.updateProps();
	}

	public void shutDown() {
		saveToFile(dbPathName);
	}

	private List<Receipt> loadFromFile(String filePath) {
		// TODO Validate the file first
		File file = new File(filePath);
		StringBuffer input = new StringBuffer(50);
		List<Receipt> result = new ArrayList<Receipt>();
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine())
			{
				input.append(scanner.nextLine() + nl);
			}
		} catch (FileNotFoundException e) {
			return result;
		}
		String[] rows = input.toString().split(nl);
		for (int i=0; i<rows.length; i++)
		{
			String[] u = rows[i].split(";");
			if (u.length == 4)
			{
				u[3] = u[3].replace(',', '.'); // in case the float is written with a comma
				result.add(new Receipt(Integer.parseInt(u[0]), Integer.parseInt(u[1]),
						Integer.parseInt(u[2]), Float.parseFloat(u[3])));
			}
		}
		return result;
	}

	private void saveToFile(String savePathName) {
		StringBuffer dataString = new StringBuffer(50);
		for (Receipt r : receipts)
		{
			dataString.append(r.getYear() + ";");
			dataString.append(r.getMonth() + ";");
			dataString.append(r.getDay() + ";");
			dataString.append(r.getPrice() + nl);
		}
		
		// Creates the folder if necessary
		File folder = new File(savePathName.substring(0, savePathName.lastIndexOf("\\")));
		if (!folder.exists())
		{
			folder.mkdirs();
		}
		
		File file = new File(savePathName);
		try {
			Writer out = new BufferedWriter(new FileWriter(file));
			out.write(dataString.toString());
			out.close();
		} catch (IOException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	private void saveToFile() {
		StringBuffer dataString = new StringBuffer(50);
		for (Receipt r : receipts)
		{
			dataString.append(r.getYear() + ";");
			dataString.append(r.getMonth() + ";");
			dataString.append(r.getDay() + ";");
			dataString.append(r.getPrice() + "\r\n");
		}
		
		// Creates the folder if necessary
		File folder = new File(dbPathName);
		if (!folder.exists())
		{
			folder.mkdirs();
		}
		
		File file = new File(dbPathName + dbFileName);
		try {
			Writer out = new BufferedWriter(new FileWriter(file));
			out.write(dataString.toString());
			out.close();
		} catch (IOException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

	public void updateViewProps(Vector<?> data) {
		List<Float> sortedList = new ArrayList<Float>();
		float sum = 0;
		float tmp;
		for (int i=0; i<data.size(); i++)
		{
			tmp = (Float)((Vector<?>)data.elementAt(i)).elementAt(3);
			sum += tmp;
			sortedList.add(tmp);
		}
		Collections.sort(sortedList);
		
		view.setTotalPrice(round2Dec(sum));
		view.setAverage(round2Dec(sum/data.size()));
		float median = sortedList.get(sortedList.size()/2);
		if (sortedList.size() % 2 == 0)
		{
			median = (median + sortedList.get(sortedList.size()/2-1))/2;
		}
		view.setMedian(round2Dec(median));
		float max = sortedList.get(sortedList.size()-1);
		view.setMaximum(round2Dec(max));
		float min = sortedList.get(0);
		view.setMinimum(round2Dec(min));
		view.setNrOfItems(sortedList.size());
	}
	
	private float round2Dec(float number)
	{
		return Math.round(number*100)/100.0f;
	}

	public void exportToCsv() {
		int returnVal = fc.showSaveDialog(view.getFrame());
		
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			saveToFile(fc.getSelectedFile().getPath());
		}
		else
		{
		}
	}

	public void importFromCsv() {
		int returnVal = fc.showOpenDialog(view.getFrame());
		
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			loadAndShowReceipts(fc.getSelectedFile().getPath());
		}
		else
		{
		}
	}

	public void removeReceipt(int i) {
		receipts.remove(i);
	}
}
