package se.chalmers.pebjorn.receiptmanager.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class AboutWindow extends JFrame {
	public AboutWindow() {
		setTitle("About");
		setBounds(new Rectangle(100, 100, 550, 400));
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblThisIsThe = new JLabel("About this program");
		lblThisIsThe.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(lblThisIsThe);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		JTextArea txtrVersionalphaDeveloper = new JTextArea();
		txtrVersionalphaDeveloper.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtrVersionalphaDeveloper.setFocusable(false);
		txtrVersionalphaDeveloper.setLineWrap(true);
		txtrVersionalphaDeveloper.setWrapStyleWord(true);
		txtrVersionalphaDeveloper.setText("Version: [v0.5] (2012-05-24)\r\nDeveloper: Plankter Productions\r\n\r\nDescription:\r\nThis application is supposed to be used by people who are interested in learning a little bit more about their economy.\r\nYou can add a receipt by selecting the date and the price, and see for example the total amount of money spent, the average cost per receipt, median, max, min and so on.\r\nTo remove a receipt, select it and use right-click.\r\nAll receipts are saved in a csv (comma-separated valus) database inbetween sessions so you don't have to worry about forgetting to save before pressing the exit button. You can also import and export csv databases using the menu.\r\n\r\nRemember that this application is still a work in progress, and that it is only an alpha version as of now. If you have suggestions, feel free to tell me! I will try to make this application better by time.\r\n\r\nAnd yes: The graphical design will change.");
		scrollPane.setViewportView(txtrVersionalphaDeveloper);
	}
}
