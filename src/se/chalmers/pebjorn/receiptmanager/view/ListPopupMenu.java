package se.chalmers.pebjorn.receiptmanager.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;


@SuppressWarnings("serial")
public class ListPopupMenu extends JPopupMenu{
	
	JMenuItem itemRemove;
	public ListPopupMenu(final KvittoApp view, final List<Integer> rowIndices)
	{
		itemRemove = new JMenuItem("Remove selected receipts");
		itemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.removeListIndices(rowIndices);
			}
		});
		add(itemRemove);
	}
}