package se.chalmers.pebjorn.receiptmanager.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import se.chalmers.pebjorn.receiptmanager.controller.Controller;
import se.chalmers.pebjorn.receiptmanager.model.Receipt;
import se.chalmers.pebjorn.receiptmanager.model.Utilities;

public class KvittoApp {

	private Controller controller;
	
	private JFrame frmDetKvittar;
	private JTable infoTable;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private Calendar today = Calendar.getInstance();
	private JSpinner spinnerYear;
	private JSpinner spinnerMonth;
	private JSpinner spinnerDay;
	private JFormattedTextField txtPrice;
	private JLabel lblTotalPrice;
	private JLabel lblAveragePrice;
	private JLabel lblMedian;
	private JLabel lblMax;
	private JLabel lblMin;
	private JLabel lblNrOfItems;
	private JRadioButton rdbtnAllReceipts;
	private JRadioButton rdbtnSelection;

	/**
	 * Create the application.
	 */
	public KvittoApp() {
		initialize();
		new Controller(this);
		new SelectionListener(infoTable, this);
		updateProps();
		frmDetKvittar.setVisible(true);
		
		// TODO Kunna s�tta intervall med hj�lp av en dialogruta (via knapp)
		// TODO Lista ut n�t s�tt att kunna anv�nda flera olika "sessions" (New-knappen)?
		// TODO Meddela med tillf�llig text n�r kvitto �r tillagt
		// TODO Add sort button
		// TODO Nollst�lla kostnadsrutan n�r ett kvitto l�ggs till
		// TODO Jobba med SciGraph och anv�nd det f�r att visualisera statistik
		// TODO Kolla om SimpleXML b�r anv�ndas ist�llet?
		// TODO Spara filen varje g�ng n�got �ndras ist�llet f�r vid shutdown
		// TODO Add button that sets the date to today
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("serial")
	private void initialize() {
		frmDetKvittar = new JFrame();
		frmDetKvittar.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				controller.shutDown();
			}
		});
		frmDetKvittar.setTitle("ReceiptManager [v0.5]");
		frmDetKvittar.setBounds(100, 100, 560, 600);
		frmDetKvittar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDetKvittar.setLocationRelativeTo(null);
		
		JMenuBar menuBar = new JMenuBar();
		frmDetKvittar.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("New...");
		mntmNew.setToolTipText("Not yet implemented");
		mntmNew.setEnabled(false);
		mnFile.add(mntmNew);
		
		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);
		
		JMenuItem mntmImportCsv = new JMenuItem("Import CSV...");
		mntmImportCsv.setToolTipText("Import a database from a file");
		mntmImportCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.importFromCsv();
			}
		});
		mnFile.add(mntmImportCsv);
		
		JMenuItem mntmExportCsv = new JMenuItem("Export CSV...");
		mntmExportCsv.setToolTipText("Export this database to a file");
		mntmExportCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.exportToCsv();
			}
		});
		mnFile.add(mntmExportCsv);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setToolTipText("Exit the application");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.shutDown();
				System.exit(0);
			}
		});
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		mnFile.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmAddReceipt = new JMenuItem("Add receipt");
		mntmAddReceipt.setToolTipText("Not yet implemented");
		mntmAddReceipt.setEnabled(false);
		mnEdit.add(mntmAddReceipt);
		
		JMenuItem mntmSetInterval = new JMenuItem("Set interval");
		mntmSetInterval.setToolTipText("Not yet implemented");
		mntmSetInterval.setEnabled(false);
		mnEdit.add(mntmSetInterval);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mntmHelp.setToolTipText("Not yet implemented");
		mntmHelp.setEnabled(false);
		mnHelp.add(mntmHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AboutWindow();
			}
		});
		mnHelp.add(mntmAbout);
		frmDetKvittar.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		frmDetKvittar.getContentPane().add(splitPane);
		
		JPanel panel = new JPanel();
		splitPane.setRightComponent(panel);
		panel.setLayout(null);
		
		JButton btnSetInterval = new JButton("Set interval");
		btnSetInterval.setToolTipText("Not yet implemented");
		btnSetInterval.setEnabled(false);
		btnSetInterval.setBounds(10, 163, 100, 23);
		panel.add(btnSetInterval);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Add receipt", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 11, 318, 123);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		NumberFormat format = NumberFormat.getNumberInstance();
		txtPrice = new JFormattedTextField(format);
		txtPrice.setToolTipText("Enter a price with comma as decimal separator");
		txtPrice.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				try {
					txtPrice.commitEdit();
				} catch (ParseException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
				if (((Number)txtPrice.getValue()).floatValue() < 0)
				{
					txtPrice.setValue(0);
				}
			}
		});
		txtPrice.setValue(0);
		txtPrice.setBounds(10, 92, 55, 20);
		panel_2.add(txtPrice);
		
		JLabel lblKr = new JLabel("Price");
		lblKr.setBounds(10, 76, 52, 14);
		panel_2.add(lblKr);
		
		JLabel lblKr_1 = new JLabel("kr");
		lblKr_1.setBounds(74, 95, 22, 14);
		panel_2.add(lblKr_1);
		
		
		spinnerYear = new JSpinner();
		((JSpinner.DefaultEditor)spinnerYear.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO G�ra s� att hela textf�ltet blir markerat
				System.out.println("spinnerYear.focusGained()"); // Blir aldrig anropat...
				((JSpinner.DefaultEditor)spinnerYear.getEditor()).getTextField().selectAll();
			}
		});
		spinnerYear.setModel(new SpinnerNumberModel(today.get(Calendar.YEAR), 1970, today.get(Calendar.YEAR), 1));
		JSpinner.NumberEditor ne_spinnerYear = new JSpinner.NumberEditor(spinnerYear, "#");
		spinnerYear.setEditor(ne_spinnerYear);
		spinnerYear.setBounds(10, 43, 55, 20);
		panel_2.add(spinnerYear);
		
		spinnerMonth = new JSpinner();
		spinnerMonth.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Calendar cal = new GregorianCalendar((Integer)spinnerYear.getValue(), (Integer)spinnerMonth.getValue()-1, 1);
				int day = Utilities.clampInt((Integer)spinnerDay.getValue(), 1, cal.getActualMaximum(Calendar.DATE));
				SpinnerModel model = new SpinnerNumberModel(day, 1, cal.getActualMaximum(Calendar.DATE), 1);
				spinnerDay.setModel(model);
			}
		});
		spinnerMonth.setModel(new SpinnerNumberModel(today.get(Calendar.MONTH)+1, 1, 12, 1));
		spinnerMonth.setBounds(74, 43, 39, 20);
		panel_2.add(spinnerMonth);
		
		spinnerDay = new JSpinner();
		spinnerDay.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				System.out.println("spinnerDay.focusGained()");
			}
		});
		spinnerDay.setModel(new SpinnerNumberModel(today.get(Calendar.DATE), 1, today.getActualMaximum(Calendar.DATE), 1));
		spinnerDay.setBounds(125, 43, 39, 20);
		panel_2.add(spinnerDay);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(10, 27, 55, 14);
		panel_2.add(lblYear);
		
		JLabel lblMonth = new JLabel("Month");
		lblMonth.setBounds(74, 28, 45, 14);
		panel_2.add(lblMonth);
		
		JLabel lblNewLabel = new JLabel("Day");
		lblNewLabel.setBounds(125, 28, 42, 14);
		panel_2.add(lblNewLabel);
		
		JButton btnAddReceipt = new JButton("");
		btnAddReceipt.setToolTipText("Add receipt");
		btnAddReceipt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.addReceipt((Integer)spinnerYear.getValue(), (Integer)spinnerMonth.getValue(), (Integer)spinnerDay.getValue(), (Number)txtPrice.getValue());
				updateProps();
			}
		});
		btnAddReceipt.setBounds(128, 76, 36, 36);
		panel_2.add(btnAddReceipt);
		// TODO Fixa tab-order
		btnAddReceipt.setIcon(new ImageIcon(KvittoApp.class.getResource("/se/chalmers/pebjorn/receiptmanager/view/add-icon32px.png")));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 189, 318, 61);
		panel.add(panel_3);
		
		rdbtnAllReceipts = new JRadioButton("All receipts");
		rdbtnAllReceipts.setToolTipText("Show information for all receipts");
		rdbtnAllReceipts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateProps();
			}
		});
		panel_3.setLayout(new GridLayout(0, 2, 0, 0));
		panel_3.add(rdbtnAllReceipts);
		buttonGroup.add(rdbtnAllReceipts);
		rdbtnAllReceipts.setVerticalTextPosition(SwingConstants.TOP);
		rdbtnAllReceipts.setHorizontalTextPosition(SwingConstants.CENTER);
		rdbtnAllReceipts.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnAllReceipts.setSelected(true);
		
		rdbtnSelection = new JRadioButton("Use selection");
		rdbtnSelection.setToolTipText("Show information only for selected receipts");
		rdbtnSelection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateProps();
			}
		});
		panel_3.add(rdbtnSelection);
		buttonGroup.add(rdbtnSelection);
		rdbtnSelection.setHorizontalTextPosition(SwingConstants.CENTER);
		rdbtnSelection.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnSelection.setVerticalTextPosition(SwingConstants.TOP);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(10, 261, 318, 159);
		panel.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 3, 0, 0));
		
		JLabel lblTitleTotalPrice = new JLabel("Total price");
		lblTitleTotalPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleTotalPrice.setToolTipText("Total price of all receipts");
		panel_4.add(lblTitleTotalPrice);
		lblTitleTotalPrice.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblTitleAveragePrice = new JLabel("Average");
		lblTitleAveragePrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleAveragePrice.setToolTipText("Average value");
		panel_4.add(lblTitleAveragePrice);
		lblTitleAveragePrice.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblTitleMedian = new JLabel("Median");
		lblTitleMedian.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleMedian.setToolTipText("Median value");
		panel_4.add(lblTitleMedian);
		lblTitleMedian.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblTotalPrice = new JLabel("");
		lblTotalPrice.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		lblTotalPrice.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTotalPrice.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblTotalPrice);
		
		lblAveragePrice = new JLabel("");
		lblAveragePrice.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		lblAveragePrice.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAveragePrice.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblAveragePrice);
		
		lblMedian = new JLabel("");
		lblMedian.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		lblMedian.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMedian.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblMedian);
		
		JLabel lblTitleMax = new JLabel("Max");
		lblTitleMax.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleMax.setToolTipText("Maximum value");
		panel_4.add(lblTitleMax);
		lblTitleMax.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblTitleMin = new JLabel("Min");
		lblTitleMin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleMin.setToolTipText("Minimum value");
		panel_4.add(lblTitleMin);
		lblTitleMin.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblTitleNbrOfItems = new JLabel("Amount");
		lblTitleNbrOfItems.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitleNbrOfItems.setToolTipText("Amount of receipts");
		lblTitleNbrOfItems.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblTitleNbrOfItems);
		
		lblMax = new JLabel("");
		lblMax.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMax.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblMax);
		
		lblMin = new JLabel("");
		lblMin.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMin.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblMin);
		
		lblNrOfItems = new JLabel("");
		lblNrOfItems.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNrOfItems.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblNrOfItems);
		
		JPanel panel_1 = new JPanel();
		splitPane.setLeftComponent(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		infoTable = new JTable();
		infoTable.setToolTipText("Right-click to remove a receipt");
		infoTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				showListPopup(e);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				showListPopup(e);
			}
		});
		infoTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPane.setViewportView(infoTable);
		infoTable.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"Year", "Month", "Day", "Price"
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, Integer.class, Integer.class, Float.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		infoTable.getColumnModel().getColumn(0).setPreferredWidth(35);
		infoTable.getColumnModel().getColumn(1).setPreferredWidth(45);
		infoTable.getColumnModel().getColumn(2).setPreferredWidth(32);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(infoTable.getModel());
		infoTable.setRowSorter(sorter);
		splitPane.setDividerLocation(200);
	}

	protected void showListPopup(MouseEvent e) {
		if (e.isPopupTrigger())
		{
			int[] unsortedRowIndices = infoTable.getSelectedRows();
			if (unsortedRowIndices.length > 0)
			{
				ListPopupMenu popupMenu = new ListPopupMenu(this, convertRowIndices(unsortedRowIndices));
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}

	public void appendToList(Receipt receipt) {
		((DefaultTableModel)infoTable.getModel()).addRow(new Object[]
				{ receipt.getYear(), receipt.getMonth(), receipt.getDay(), receipt.getPrice() });
	}

	public void clearList() {
		((DefaultTableModel)infoTable.getModel()).setRowCount(0);
	}

	public void setTotalPrice(float totalPrice) {
		// TODO Make these shown in currency format instead of number format.
		lblTotalPrice.setText("" + totalPrice + " kr");
	}

	public void setAverage(float average) {
		lblAveragePrice.setText("" + average + " kr");
	}

	public void setMedian(float median) {
		lblMedian.setText("" + median + " kr");
	}

	public void setMaximum(float max) {
		lblMax.setText("" + max + " kr");
	}

	public void setMinimum(float min) {
		lblMin.setText("" + min + " kr");
	}

	public void setNrOfItems(int nr) {
		lblNrOfItems.setText("" + nr);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updateProps() {
		Vector data = ((DefaultTableModel)infoTable.getModel()).getDataVector();
		if (data.size() > 0)
		{
			if (rdbtnAllReceipts.isSelected())
			{
				controller.updateViewProps(data);
			}
			else
			{
				int[] unsortedRowIndices = infoTable.getSelectedRows();
				if (unsortedRowIndices.length > 0)
				{
					List<Integer> rowIndices = convertRowIndices(unsortedRowIndices);
					Vector output = new Vector();
					for (Integer i : rowIndices)
					{
						output.add(data.elementAt(i));
					}
					controller.updateViewProps(output);
				}
			}
		}
	}

	private List<Integer> convertRowIndices(int[] rowIndices) {
		List<Integer> output = new ArrayList<Integer>();
		for (int i=0; i<rowIndices.length; i++)
		{
			output.add(infoTable.convertRowIndexToModel(rowIndices[i]));
		}
		return output;
	}

	/**
	 * @return the frmDetKvittar
	 */
	public JFrame getFrame() {
		return frmDetKvittar;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	public void removeListIndices(List<Integer> rowIndices) {
		int choice = JOptionPane.showConfirmDialog(getFrame(),
				"Do you really want to remove the receipts?\nThere is no way to get them back if you click 'Yes'.",
				"Confirm removal",
				JOptionPane.YES_NO_OPTION);
		
		if (choice == JOptionPane.YES_OPTION)
		{
			Collections.sort(rowIndices);
			Collections.reverse(rowIndices); // Reverse order to use the indices backwards
			for (Integer i : rowIndices)
			{
				// TODO Do this the other way around, let the controller manage what's going away
				controller.removeReceipt(i);
				((DefaultTableModel)infoTable.getModel()).removeRow(i);
			}
		}
	}
}
