package se.chalmers.pebjorn.receiptmanager.view;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class SelectionListener implements ListSelectionListener {

	private KvittoApp view;
	private JTable table;
	// It is necessary to keep the table since it is not possible
    // to determine the table from the event's source
    public SelectionListener(JTable table, KvittoApp view) {
        this.table = table;
        table.getSelectionModel().addListSelectionListener(this);
        table.getColumnModel().getSelectionModel().addListSelectionListener(this);
        this.view = view;
    }
    
	@Override
	public void valueChanged(ListSelectionEvent e) {
        // If cell selection is enabled, both row and column change events are fired
        if (e.getSource() == table.getSelectionModel()
              && table.getRowSelectionAllowed()) {
        	view.updateProps();
            // Column selection changed
            //int first = e.getFirstIndex();
            //int last = e.getLastIndex();
        } else if (e.getSource() == table.getColumnModel().getSelectionModel()
               && table.getColumnSelectionAllowed() ){
            // Row selection changed
            //int first = e.getFirstIndex();
            //int last = e.getLastIndex();
        }

        if (e.getValueIsAdjusting()) {
            // The mouse button has not yet been released
        }
    }
}